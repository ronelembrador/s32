const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is blank. Please update"]
	},

	lastName: {
		type: String,
		required: [true, "Last name is blank, Please update."]
	},

	email: {
		type: String,
		required: [true, "Please enter your email"]
	},

	password: {
		type: String,
		required: [true, "Please enter your password"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	mobileNo: {
		type: String,
		required: [true, "Please enter your mobile number"]
	},

	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Please enter the course ID"]
			},

			enrolledOn: {
				type: Date,
				default: new Date()
			},

			status: {
				type: String,
				default: "enrolled"
			}
		}]

})

module.exports = mongoose.model("User", userSchema);