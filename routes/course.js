const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth")



router.post("/", (req, res) => {
	const courseData = auth.decode(req.headers.authorization)

	if (courseData.isAdmin === true) {
	courseController.addCourse(req.body, {userId: req.body.id}).then(resultFromController => 
		res.send(resultFromController));
	} else {
		res.send({auth: "You do not have sufficient authorization to do this."})		
	}

})


// Retrieve all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
});


// Retrieve all active course
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Retrieve specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params)
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// Update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


// Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
// Archive a course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	const isUserAdmin = auth.decode(req.headers.authorization)


	if (isUserAdmin.isAdmin === true) {
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You do not have sufficient authorization to do this.`)
	}
})


module.exports = router;