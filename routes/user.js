const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth")


router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => 
		res.send(resultFromController))

})

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController =>
		res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController =>
		res.send(resultFromController));

})

// 1. Create a /details route that will accept the user's ID to retrieve the details of a user
router.get("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId: req.body.id}).then(resultFromController =>
		res.send(resultFromController));
})


// For enrolling a user
router.post("/enroll", auth.verify, (req, res) => {


	// 1. Refactor the user route to implement user authentication for the enroll route
	const isUserLoggedIn = auth.decode(req.headers.authorization)

	if (isUserLoggedIn.isAdmin === false) {
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

	} else {
		res.send(`You are not authorized for this action`)
	}
})




module.exports = router;