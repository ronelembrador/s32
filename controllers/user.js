const User = require("../models/User");
const Course = require("../models/Course")
const bcrypt = require("bcrypt");
const auth = require("../auth")

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return true
		} else {
			return false
		}

	})

}

module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
		// Syntax: 
			// bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})

}


// Login

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null){
			return false;
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}


	})
}

// create a getProfile controller method for retrieving the details of the user.
// Details

module.exports.getProfile = (data) => {



	return User.findById(data.userId).then(result => {

		result.password = "";
		return result;

	})
}


// For enrolling user

// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user.
module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})

// Using the "await" keyword will allow the enroll method to complete the course before returning a response back
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId: data.userId})
		// save the updated course information 
		return course.save().then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})

// Condition that will check if the user and course documents have been updated
		// User enrollment is successful
		if(isUserUpdated && isCourseUpdated) {
			return true
		} 

		// User enrollment failure
		else {
			return false
		}

}