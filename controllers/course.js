const Course = require("../models/Course");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth")
const jwt = require("jsonwebtoken");


module.exports.addCourse = (reqBody) => {

		let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	})

	
	return newCourse.save().then((course, error) => {



		if(error) {
			return false
		} else {
			return true
		}
	})

}

// Retrieve all the courses
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
}

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}


// Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}


// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	// Syntax:
		// findByIdAndUpdate(document, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}


// 2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body
// Archive a course
module.exports.archiveCourse = (reqParams, reqBody) => {

	let archivedCourse = {isActive: reqBody.isActive}
	
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error){
			return `Something is wrong. Please check and try again.`;
		} else {
			return `Course archiving is successful.`
		}
	})
}